#!/usr/bin/env bash
# audio-stream -- Transmisión de audio
#
# © 2020 barca infraestructura pirata
#
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.
#
# http://www.gnu.org/prep/maintain/html_node/License-Notices-for-Other-Files.html

set -e
set -E

PROGRAM="$(basename -- "$0")"
ICECAST_PORT=${ICECAST_PORT:-8000}
ICECAST_SERVER=${ICECAST_SERVER:-transmisor.partidopirata.com.ar}
ICECAST_PASSWORD=${ICECAST_PASSWORD:-hackme}
ICECAST_USER=${ICECAST_USER:-source}
ICECAST_CHANNEL=${ICECAST_CHANNEL:-${PROGRAM}}

VERBOSE=false


#link para escuchar el recurso
function get_public_url() {
  echo "https://${ICECAST_SERVER}/${ICECAST_CHANNEL}.ogg"
}
#link para enviar transmitir
function get_icecast_url() {
  echo "icecast://${ICECAST_USER}:${ICECAST_PASSWORD}@${ICECAST_SERVER}:${ICECAST_PORT}/${ICECAST_CHANNEL}.ogg"
}

#con esto se captura el audio
function transmitir() {
#  local _resolution=$1 ; shift
  ${VERBOSE} || local _quiet="-loglevel quiet"

  # g = framerate * segundos (5, 10)
  # cluster size = más alto que el ancho de banda máximo (?)
  # cluster time = segundos

  ffmpeg \
${_quiet} \
  -f alsa \
  -i default \
  -content_type audio/ogg \
  -c:a libvorbis \
  -b:a 96K \
$(get_icecast_url)
}

# Chequear que no estemos sourceando
function is_main() {
  test 'xaudio-stream' = "x${PROGRAM}"
}

function help() {
  echo "${PROGRAM} -- Transmitir audio stream"
  echo
  echo 'Uso:'
  echo
  echo "  [ICECAST_PASSWORD=${ICECAST_PASSWORD}] ./${PROGRAM} [-vh] [-S ${SIZE}] [-A] [-s ${ICECAST_SERVER}] [-p ${ICECAST_PORT}] [-c ${ICECAST_CHANNEL}] [-u ${ICECAST_USER}] [-j ${JITSI}] [-k]"
  echo
  echo "La contraseña siempre se pasa como variable de entorno."
  echo
  echo "-v Verborragia"
  echo "-h Ayuda"
  echo "-s Dirección del servidor"
  echo "-p Puerto"
  echo "-c Canal (\"source\")"
  echo "-k Modo kiosko"
  echo
  echo "Con 'q' terminás de transmitir"
}

function main() {
  while getopts hvu:c:s:p:j:S:Ak option; do
    case ${option} in
      h) help ; exit ;;
      v) VERBOSE=true ;;
      u) ICECAST_USER="${OPTARG}" ;;
      p) ICECAST_PORT="${OPTARG}" ;;
      s) ICECAST_SERVER="${OPTARG}" ;;
      c) ICECAST_CHANNEL="${OPTARG}" ;;
      k) KIOSK="-kiosk" ;;
    esac
  done
  shift $((${OPTIND} -1 ))

  ${VERBOSE} && echo -n 'Tu transmisión de audio saldrá por '
  get_public_url
  transmitir ${SIZE}
}

# Ejecutar el programa si no estamos sourceando
is_main && main $@
