#!/usr/bin/env bash
# tv-pirata -- Transmisión de escritorio
#
# © 2017 fauno <fauno@partidopirata.com.ar>
#
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.
# 
# http://www.gnu.org/prep/maintain/html_node/License-Notices-for-Other-Files.html

set -e
set -E
set -x

PROGRAM="$(basename -- "$0")"
FILE="./local.webm"
VERBOSE=false

# Obtiene la resolución actual
function get_resolution() {
  xrandr | grep '*' | tr -s ' ' | cut -d' ' -f2
}

function transmitir() {
  local _resolution=$1 ; shift
  ${VERBOSE} || local _quiet="-loglevel quiet"

  ffmpeg -video_size ${_resolution} \
          -framerate 25 \
          -f x11grab \
          -i ${DISPLAY}+0,0 \
          -f alsa \
          -thread_queue_size 1024 \
          -i default \
          -f webm \
          -cluster_size_limit 2M \
          -cluster_time_limit 2000 \
          -content_type video/webm \
          -c:a libvorbis \
          -b:a 64K \
          -c:v libvpx \
          -b:v 400K \
          -crf 30 \
          -g 150 \
          -deadline good \
          -threads 4 \
          "$FILE"
}

# Chequear que no estemos sourceando
function is_main() {
  test 'xlocal' = "x${PROGRAM}"
}

function help() {
  echo "${PROGRAM} -- Transmitir el escritorio"
  echo
  echo 'Uso:'
  echo
  echo "  ./${PROGRAM} [-vh] [-f ARCHIVO]"
  echo
  echo "La contraseña siempre se pasa como variable de entorno."
  echo
  echo "Con 'q' terminás de transmitir"
}

function main() {
  local _resolution=$(get_resolution)

  while getopts hvf: option; do
    case ${option} in
      h) help ; exit ;;
      v) VERBOSE=true ;;
      f) FILE="${OPTARG}" ;;
    esac
  done
  shift $((${OPTIND} -1 ))

  transmitir ${_resolution}
}

# Ejecutar el programa si no estamos sourceando
is_main && main $@
